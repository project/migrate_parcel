A system to keep 'structural' entities in code, using Migrate to import them and inspired by Features for their storage.

This is intended to be used with entities that are treated as part of a site's structure rather than content. For example, on drupal.org this might be the taxonomy terms for major versions, '6.x', '7.x' and so on.

Migrations are defined automatically with an .info file and a CSV file of entities. The CSV file headers are assumed to match the key schema for the entity being imported.

Specification
-------------

An entity import migration is defined having a folder and file structure similar to a module in sites/all/data:

- sites/all/data/my_import
- sites/all/data/my_import.info
- sites/all/data/my_import.csv

The info file has the following properties:

name = The human-readable name. Appears in the Migrate UI.
description = The human-readable description. Appears in the Migrate UI.
entity = The machine name of the entity type to use.
bundle = The machine name of the entity bundle to use.
; The following are optional:
; Define one or more mapping default values:
mapping_defaults[pathauto] = 0
; Source key for the MigrateSQLMap. If omitted, the entity primary key is
; used (which means your CSV must have it, though it is not used for import)
key = name

TODO
----

- Exporting entities to a CSV file to complete the round trip
- look into whether this should be a Features component rather than standalone
