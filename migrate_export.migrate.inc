<?php
/**
 * @file migrate_export.migrate.inc
 * Contains hooks and classes for migrate.
 */

/**
 * Implements hook_migrate_api().
 */
function migrate_export_migrate_api() {
  $api = array(
    'api' => 2,
  );
  return $api;
}

/**
 * Our main migration class.
 */
class MigrateExportMigration extends DynamicMigration {

  /**
   * @param $arguments
   *  An array of arguments. Expects:
   *  - machine_name: The machine name to use.
   *  - description: Description of the migration data.
   *  - source_file: The source CSV file.
   *  - entity: The entity type.
   *  - bundle: The bundle machine name.
   *  - mapping_defaults: (optional) An array of default values for mappings,
   *    keyed by destination field, where the value is the default to use.
   */
  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    $entity_type = $arguments['entity'];
    $bundle_name = $arguments['bundle'];

    parent::__construct();

    // Human-friendly description of this migration process.
    $this->description = $arguments['description'];

    // Get the destination class.
    $destination_class = $this->getDestinationClass($arguments['entity']);

    // Create a map object for tracking the relationships between source rows.
    if ($destination_class == 'MigrateDestinationEntityAPI') {
      $map_key_schema_destination = MigrateDestinationEntityAPI::getKeySchema($entity_type);
    }
    else {
      $map_key_schema_destination = $destination_class::getKeySchema();
    }

    if (isset($arguments['key'])) {
      $key = $arguments['key'];
      $map_key_schema_source = array(
        $key => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => "Product mapping ID from Robert's CSV",
        ),
      );
    }
    else {
      // If no key is given in the arguments, use the same map schema for both
      // source and destination: assume the CSV file has a column for the entity
      // primary key.
      $map_key_schema_source = $map_key_schema_destination;
    }

    $this->map = new MigrateSQLMap($this->machineName,
      $map_key_schema_source,
      $map_key_schema_destination
    );

    // Unlike normal Migration classes, the source of the data is not hard-coded
    // - it comes from the arguments
    $source_options = array('header_rows' => 1);
    $this->source = new MigrateSourceCSV($arguments['source_file'], array(), $source_options, array());

    // Set up our destination.
    // Parameters are different depending on the class used.
    if ($destination_class == 'MigrateDestinationEntityAPI') {
      $this->destination = new MigrateDestinationEntityAPI(
        $entity_type,
        $bundle_name
      );
    }
    else {
      $this->destination = new $destination_class(
        $bundle_name
      );
    }

    $destination_fields = array_keys($this->destination->fields());
    $source_fields = array_keys($this->source->fields());
    $common_fields = array_intersect($destination_fields, $source_fields);
    $this->addSimpleMappings($common_fields);

    if (isset($this->arguments['mapping_defaults'])) {
      foreach ($this->arguments['mapping_defaults'] as $field_name => $default_value) {
        $this->addFieldMapping($field_name, NULL)
          ->defaultValue($default_value)
          ->description(t('Default value from info file.'));
      }
    }
  }

  /**
   * Construct the machine name.
   */
  protected function generateMachineName($class_name = NULL) {
    return $this->arguments['machine_name'];
  }

  /**
   * Get the destination class.
   *
   * @param $entity_type
   *  The type of entity to get the class for.
   */
  function getDestinationClass($entity_type) {
    $lookup = array(
      'node' => 'MigrateDestinationNode',
      'taxonomy_term' => 'MigrateDestinationTerm',
    );
    if (isset($lookup[$entity_type])) {
      return $lookup[$entity_type];
    }
    return 'MigrateDestinationEntityAPI';
  }

}
